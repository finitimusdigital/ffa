package io.github.kabryxis.finitmusffa.command.listener;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import io.github.kabryxis.finitmusffa.FFA;
import io.github.kabryxis.finitmusffa.command.CommandFramework.Command;
import io.github.kabryxis.finitmusffa.command.CommandFramework.CommandArgs;
import io.github.kabryxis.finitmusffa.kit.Kit;

public class CommandListener {
	
	private final FFA plugin;
	
	public CommandListener(FFA plugin) {
		this.plugin = plugin;
	}
	
	@Command(name = "kit", description = "Choose a kit.", permission = "ffa.kit")
	public void onKit(CommandArgs arg) {
		if(!arg.isPlayer()) {
			arg.getSender().sendMessage("You must be a player to use this command.");
			return;
		}
		String[] args = arg.getArgs();
		if(args.length == 1) {
			Player player = arg.getPlayer();
			Kit kit = plugin.getKit(args[0]);
			if(kit == null || !kit.hasPermission(player)) {
				player.sendMessage("The kit '" + args[0] + "' does not exist.");
				return;
			}
			if(player.hasPermission("ffa.kitall")) {
				for(Player p : plugin.getServer().getOnlinePlayers()) {
					if(!p.getName().equals(player.getName())) {
						kit.give(p);
						p.sendMessage("You have received the kit '" + args[0] + "' from " + player.getName() + ".");
					}
				}
				kit.give(player);
				player.sendMessage("You have given everyone the kit '" + args[0] + "' including yourself.");
			}
			else {
				kit.give(player);
				player.sendMessage("You have received the kit '" + args[0] + "'.");
			}
		}
		else arg.getSender().sendMessage("Usage: /kit [kit name]");
	}
	
	@Command(name = "kits", description = "See the available kits.", permission = "ffa.kits")
	public void onKits(CommandArgs arg) {
		CommandSender sender = arg.getSender();
		sender.sendMessage(arg.getArgs().length == 0 ? plugin.getKitList(sender) : "Usage: /kits");
	}
	
	@Command(name = "kills", description = "See the amount of kills you or the specified player has.", permission = "ffa.kills")
	public void onKills(CommandArgs arg) {
		String[] args = arg.getArgs();
		CommandSender sender = arg.getSender();
		if(args.length == 0) {
			if(!arg.isPlayer()) {
				arg.getSender().sendMessage("You must be a player to use this command.");
				return;
			}
			sender.sendMessage("You have " + plugin.getKills(arg.getPlayer()) + " kills.");
		}
		else if(args.length == 1) {
			Player player = plugin.getServer().getPlayerExact(args[0]);
			if(player == null) {
				sender.sendMessage("The player '" + args[0] + "' is not online.");
				return;
			}
			sender.sendMessage(player.getName() + " has " + plugin.getKills(player) + " kills.");
		}
		else sender.sendMessage("Usage: /kills {user}");
	}
	
	@Command(name = "ffa", description = "Manage the FFA plugin.", permission = "ffa.ffa")
	public void onFFA(CommandArgs arg) {
		String[] args = arg.getArgs();
		CommandSender sender = arg.getSender();
		if(args.length == 1) {
			if(args[0].equalsIgnoreCase("reload")) {
				plugin.loadKits();
				sender.sendMessage("The kits configuration has been reloaded.");
			}
		}
		else if(args.length == 2) {
			if(args[0].equalsIgnoreCase("set")) {
				if(!arg.isPlayer()) {
					sender.sendMessage("You must be a player to use this command.");
					return;
				}
				Player player = arg.getPlayer();
				int rank;
				try {
					rank = Integer.parseInt(args[1]);
				}
				catch(NumberFormatException e) {
					sender.sendMessage("'" + args[1] + "' is not a number.");
					return;
				}
				if(rank > 0 && rank <= 5) {
					plugin.setSet(player, rank - 1);
				}
				else sender.sendMessage("Rank must be between 1 and 5.");
			}
		}
		else sender.sendMessage("Usage: /ffa [reload/set] {rank #}");
	}
	
}
