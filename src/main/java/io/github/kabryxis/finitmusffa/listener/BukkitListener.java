package io.github.kabryxis.finitmusffa.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import io.github.kabryxis.finitmusffa.FFA;

public class BukkitListener implements Listener {
	
	private final FFA plugin;
	
	public BukkitListener(FFA plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		player.teleport(plugin.getServer().getWorld("world").getSpawnLocation());
		player.setMetadata("immunity", new FixedMetadataValue(plugin, true));
		plugin.giveDefaultKit(player);
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		plugin.unload(event.getPlayer());
	}
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			Player player = (Player)event.getEntity();
			if(player.hasMetadata("immunity")) {
				event.setCancelled(true);
				if(event.getCause() == DamageCause.FALL && player.getLocation().getBlockY() <= plugin.getY())
					player.removeMetadata("immunity", plugin);
			}
		}
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event) {
		event.getDrops().clear();
		Player player = event.getEntity();
		Player killer = player.getKiller();
		if(killer != null) plugin.increaseKillCount(killer);
		new BukkitRunnable() {
			
			@Override
			public void run() {
				player.spigot().respawn();
			}
			
		}.runTaskLater(plugin, 1L);
	}
	
	@EventHandler
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		Player player = event.getPlayer();
		player.setVelocity(new Vector(0, 0, 0));
		new BukkitRunnable() {
			
			@Override
			public void run() {
				player.setVelocity(new Vector(0, 0, 0));
				plugin.giveDefaultKit(player);
				player.setMetadata("immunity", new FixedMetadataValue(plugin, true));
			}
			
		}.runTaskLater(plugin, 1L);
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Integer rank = plugin.getSet(event.getPlayer());
			if(rank != null) {
				event.setCancelled(true);
				plugin.setNewBlock(rank.intValue(), event.getClickedBlock());
			}
		}
	}
	
	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent event) {
		event.setCancelled(true);
		if(event.getFoodLevel() < 20) event.setFoodLevel(20);
	}
	
}
