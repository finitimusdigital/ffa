package io.github.kabryxis.finitmusffa.data;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

public class Config extends YamlConfiguration {
	
	private final File file;
	
	public Config(String path) {
		this.file = new File(path + ".yml");
		if(file.exists()) load();
	}
	
	public void load() {
		try {
			load(file);
		}
		catch(IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	public void save() {
		try {
			save(file);
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	
}
