package io.github.kabryxis.finitmusffa.kit;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.permissions.Permissible;

public class Kit {
	
	private final ItemStack[] armor = new ItemStack[4];
	private final ItemStack[] inventory = new ItemStack[36];
	
	private String permission = "";
	
	public Kit(ConfigurationSection section) {
		build(section);
	}
	
	public void give(Player player) {
		PlayerInventory inv = player.getInventory();
		inv.setArmorContents(armor);
		inv.setContents(inventory);
	}
	
	public boolean hasPermission(Permissible perm) {
		return permission.isEmpty() ? true : perm.hasPermission(permission);
	}
	
	private void build(ConfigurationSection section) {
		if(section.isConfigurationSection("armor")) setArmor(section.getConfigurationSection("armor"));
		if(section.isConfigurationSection("inventory")) setInventory(section.getConfigurationSection("inventory"));
		permission = section.getString("permission", "");
	}
	
	private void setArmor(ConfigurationSection section) {
		section.getKeys(false).forEach(key -> {
			armor[id(key)] = buildItem(section.getConfigurationSection(key));
		});
	}
	
	private void setInventory(ConfigurationSection section) {
		section.getKeys(false).forEach(key -> {
			if(!key.equalsIgnoreCase("rest")) inventory[Integer.parseInt(key)] = buildItem(section.getConfigurationSection(key));
		});
		if(section.isConfigurationSection("rest")) {
			ItemStack item = buildItem(section.getConfigurationSection("rest"));
			for(int i = 0; i < 36; i++) {
				if(inventory[i] == null) inventory[i] = item;
			}
		}
	}
	
	private ItemStack buildItem(ConfigurationSection section) {
		ItemStack item = new ItemStack(Material.getMaterial(section.getString("material")), section.getInt("amount", 1));
		int data = section.getInt("data", -1);
		if(data != -1) item.setDurability((short)data);
		List<String> enchantments = section.getStringList("enchantments");
		boolean immortal = section.getBoolean("immortal", false);
		if(enchantments != null || immortal) {
			ItemMeta meta = item.getItemMeta();
			if(enchantments != null) {
				for(String string : enchantments) {
					String[] parts = string.split(":");
					meta.addEnchant(Enchantment.getByName(parts[0]), Integer.parseInt(parts[1]), true);
				}
				enchantments.clear();
			}
			if(immortal) {
				meta.spigot().setUnbreakable(true);
				meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
			}
			item.setItemMeta(meta);
		}
		return item;
	}
	
	private int id(String armor) {
		switch(armor) {
		case "helmet":
			return 3;
		case "chestplate":
			return 2;
		case "leggings":
			return 1;
		case "boots":
			return 0;
		default:
			return -1;
		}
	}
	
}
