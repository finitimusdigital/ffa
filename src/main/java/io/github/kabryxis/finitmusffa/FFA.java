package io.github.kabryxis.finitmusffa;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.SkullType;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.block.Skull;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permissible;
import org.bukkit.plugin.java.JavaPlugin;

import io.github.kabryxis.finitmusffa.command.CommandFramework;
import io.github.kabryxis.finitmusffa.command.listener.CommandListener;
import io.github.kabryxis.finitmusffa.data.Config;
import io.github.kabryxis.finitmusffa.kit.Kit;
import io.github.kabryxis.finitmusffa.listener.BukkitListener;

public class FFA extends JavaPlugin {
	
	private final Comparator<String> comparator = new KillComparator();
	private final CommandFramework framework = new CommandFramework(this);
	
	private final Map<String, Kit> kits = new HashMap<>();
	private final Map<String, Integer> kills = new HashMap<>();
	private final Map<Player, Integer> set = new HashMap<>();
	private final Skull[] topFiveHeads = new Skull[5];
	private final Sign[] topFiveSigns = new Sign[5];
	
	private Config killsConfig;
	private Config kitsConfig;
	private List<String> topFive = new ArrayList<>();
	private Kit defaultKit;
	
	@Override
	public void onEnable() {
		killsConfig = new Config(getDataFolder() + File.separator + "kills");
		kitsConfig = new Config(getDataFolder() + File.separator + "kits");
		loadKits();
		loadKills();
		getServer().getPluginManager().registerEvents(new BukkitListener(this), this);
		framework.registerCommands(new CommandListener(this));
	}
	
	@Override
	public void onDisable() {
		kills.entrySet().forEach(entry -> killsConfig.set(entry.getKey(), entry.getValue().intValue()));
		killsConfig.set("topfive", topFive);
		killsConfig.save();
		kitsConfig.save();
	}
	
	public int getY() {
		return kitsConfig.getInt("fall-damage-y");
	}
	
	private void loadKills() {
		killsConfig.getValues(false).entrySet().forEach(entry -> {
			if(!entry.getKey().equals("topfive") && entry.getValue() != null) kills.put(entry.getKey(), (Integer)entry.getValue());
		});
		topFive = killsConfig.getStringList("topfive");
		sortAndRemove();
	}
	
	public void loadKits() {
		saveResource("kits.yml", false);
		kits.clear();
		kitsConfig.load();
		ConfigurationSection section = kitsConfig.getConfigurationSection("kits");
		section.getKeys(false).forEach(key -> {
			kits.put(key.toLowerCase(), new Kit(section.getConfigurationSection(key)));
		});
		defaultKit = getKit(kitsConfig.getString("default-kit"));
		for(int i = 1; i <= 5; i++) {
			if(kitsConfig.isSet("heads." + i)) topFiveHeads[i - 1] = (Skull)((Location)kitsConfig.get("heads." + i)).getBlock().getState();
			if(kitsConfig.isSet("signs." + i)) topFiveSigns[i - 1] = (Sign)((Location)kitsConfig.get("signs." + i)).getBlock().getState();
		}
	}
	
	public void setNewBlock(int rank, Block block) {
		if(block.getType() == Material.SIGN_POST || block.getType() == Material.WALL_SIGN) {
			Sign old = topFiveSigns[rank];
			if(old != null) {
				clearSign(old);
				old.update();
			}
			topFiveSigns[rank] = (Sign)block.getState();
			updateSign(rank);
			kitsConfig.set("signs." + (rank + 1), block.getLocation());
		}
		else if(block.getType() == Material.SKULL) {
			Skull old = topFiveHeads[rank];
			if(old != null) {
				old.setOwner("");
				old.update();
			}
			topFiveHeads[rank] = (Skull)block.getState();
			updateHead(rank);
			kitsConfig.set("heads." + (rank + 1), block.getLocation());
		}
	}
	
	private void clearSign(Sign sign) {
		for(int i = 0; i < 4; i++) {
			sign.setLine(i, "");
		}
	}
	
	public void setSet(Player player, Integer rank) {
		set.put(player, rank);
	}
	
	public Integer getSet(Player player) {
		return set.remove(player);
	}
	
	public Kit getKit(String name) {
		return kits.get(name.toLowerCase());
	}
	
	public String getKitList(Permissible perm) {
		StringBuilder builder = new StringBuilder("Available kits are: ");
		int i = 0;
		for(Entry<String, Kit> entry : kits.entrySet()) {
			if(entry.getValue().hasPermission(perm)) {
				builder.append(entry.getKey());
				if(i < kits.size() - 1) builder.append(", ");
			}
			i++;
		}
		return builder.toString();
	}
	
	public void giveDefaultKit(Player player) {
		defaultKit.give(player);
	}
	
	public int getKills(OfflinePlayer player) {
		return kills.getOrDefault(player.getUniqueId().toString(), 0).intValue();
	}
	
	public void increaseKillCount(Player player) {
		Integer k = kills.getOrDefault(player.getUniqueId().toString(), 0);
		kills.put(player.getUniqueId().toString(), (k == null ? 0 : k.intValue()) + 1);
		if(!topFive.contains(player.getName())) topFive.add(player.getName());
		sortAndRemove();
	}
	
	private void sortAndRemove() {
		Collections.sort(topFive, comparator);
		if(topFive.size() == 6) topFive.remove(5);
		updateHeadsAndSigns();
	}
	
	private void updateHeadsAndSigns() {
		for(int i = 0; i < 5; i++) {
			updateSign(i);
			updateHead(i);
		}
	}
	
	@SuppressWarnings("deprecation")
	private void updateSign(int index) {
		Sign s = topFiveSigns[index];
		s.setLine(0, "");
		s.setLine(3, "");
		if(topFive.size() >= index + 1 && topFive.get(index) != null) {
			OfflinePlayer player = getServer().getOfflinePlayer(topFive.get(index));
			s.setLine(1, player.getName());
			s.setLine(2, String.valueOf(getKills(player)));
		}
		s.update();
	}
	
	private void updateHead(int index) {
		Skull skull = topFiveHeads[index];
		skull.setSkullType(SkullType.PLAYER);
		if(topFive.size() >= index + 1 && topFive.get(index) != null) skull.setOwner(topFive.get(index));
		skull.update();
	}
	
	public void unload(Player player) {
		set.remove(player);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		return framework.handleCommand(sender, label, cmd, args);
	}
	
	class KillComparator implements Comparator<String> {
		
		@SuppressWarnings("deprecation")
		@Override
		public int compare(String name1, String name2) {
			OfflinePlayer p1 = getServer().getOfflinePlayer(name1);
			OfflinePlayer p2 = getServer().getOfflinePlayer(name2);
			return getKills(p1) > getKills(p2) ? -1 : 1;
		}
		
	}
	
}
